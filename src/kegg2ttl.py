# -*- coding: utf-8 -*-
"""
Created on Tue May 12 11:18:16 2020

@author: harta005
"""
import rdflib
import os
import re
import requests
import urllib
import time
from sys import argv
from subprocess import check_call
#os.chdir('C://Users/harta005/Projects/linked-data/kegg')

def get_gene2path_dict():
    kegg_dict = {}
    url = 'http://rest.kegg.jp/link/pathway/ath'
    f = urllib.request.urlopen(url)
    data = f.readlines()
    for line in data:
        row = line.decode('utf-8').strip('\n').split('\t')
        pathway = row[1].replace('path:ath', 'map')
        gene = row[0].strip('ath:')
        if pathway not in kegg_dict.keys():
            kegg_dict[pathway] = []
        kegg_dict[pathway].append(gene)
    return(kegg_dict)

def get_pathway():
    pathway_dict = {}
    url = 'http://rest.kegg.jp/list/path'
    f = urllib.request.urlopen(url)
    data = f.readlines()
    for line in data:
        row = line.decode('utf-8').strip('\n').split('\t')
        pathway = row[0].replace('path:', '')
        desc = row[1]
        pathway_dict[pathway] = desc
    return(pathway_dict)

def create_kegg_graph(kegg_dict, pathway_dict, output = 'kegg.ttl'):
    g = rdflib.Graph()
            
    ensembl = rdflib.Namespace('http://rdf.ebi.ac.uk/resource/ensembl/')
    path = rdflib.Namespace('https://www.genome.jp/dbget-bin/www_bget?')
    resource = rdflib.Namespace('http://semanticscience.org/resource/')
    obo = rdflib.Namespace('http://purl.obolibrary.org/obo/')
    rdf = rdflib.Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    rdfs = rdflib.Namespace('http://www.w3.org/2000/01/rdf-schema#')
        
    g.bind('ensembl', ensembl)
    g.bind('path', path)
    g.bind('resource', resource)
    g.bind('obo', obo)
    g.bind('rdf', rdf)
    g.bind('rdfs', rdfs)

    for key in kegg_dict.keys():
        g.add((
            path[key],
            rdfs.label,
            rdflib.Literal(str(pathway_dict[key]))
            ))
        for gene in kegg_dict[key]:
            g.add((
                ensembl[gene],
                resource.SIO_000255,
                path[key]
                ))
    g.serialize(destination = output, format='ttl')
    #check_call('gzip {}'.format(output))
    # create graph file
    f = open(output + '.graph', 'w')
    f.write('https://www.genome.jp/kegg/')
    f.close()
    
if __name__ == "__main__":
    output = str(argv[1])
    kegg_dict = get_gene2path_dict()
    pathway_dict = get_pathway()
    create_kegg_graph(kegg_dict, pathway_dict, output)
    check_call('gzip {}'.format(output), shell = True)
