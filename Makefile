.PHONY: all install-pkgs import-rdf update-rdf post-install

#PWD = /lustre/BIF/nobackup/harta005/ara-ld
VOS_USER = dba
VOS_PASS = dba
VOS_PORT = 1111
VOS_SHARE = /tmp/share
VOS = isql $(VOS_PORT) $(VOS_USER) $(VOS_PASS) verbose=off errors=stdout
DOCKER_IMAGE = candygene/virtuoso
CONTAINER_NAME = ara-ld2
CONTAINER_PORT = 8891

all: install-pkgs import-rdf update-rdf post-install

#pull-image:
#	docker pull $(DOCKER_IMAGE)

#start-srv:
#	docker rm -f $(CONTAINER_NAME) || exit 0
#	docker run --name $(CONTAINER_NAME) -v "$(PWD):$(VOS_SHARE)" -p $(CONTAINER_PORT):8890 -d $(DOCKER_IMAGE)
#	sleep 5
#	docker ps -a

#stop-srv:
#	docker stop $(CONTAINER_NAME)
#	docker ps -a

#restart-srv:
#	docker restart $(CONTAINER_NAME)
#	docker ps -a

install-pkgs:
	docker exec $(CONTAINER_NAME) $(VOS) install_pkgs.sql

import-rdf:
#	sed -i.org "s:__DATA_DIR__:$(DATA_DIR):" import_rdf.sql
	docker exec $(CONTAINER_NAME) $(VOS) import_rdf.sql

update-rdf:
	docker exec $(CONTAINER_NAME) $(VOS) update_rdf.sql

post-install:
	docker exec $(CONTAINER_NAME) $(VOS) post_install.sql

