#!/bin/bash

import-rdf:
	sed -i.org "s:__DATA_DIR__:data" import_rdf.sql
	docker exec virtuoso-test2 1111 dba dba import_rdf.sql

update-rdf:
	docker exec virtuoso-test2 1111 dba dba update_rdf.sql

post-install:
	docker exec virtuoso-test2 1111 dba dba post_install.sql


