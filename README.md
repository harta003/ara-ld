# ara-ld
This work is based on [pbg-ld](https://doi.org/10.5281/zenodo.3385231): [Linked Data Platform for Plant Breeding & Genomics](https://www.mdpi.com/2076-3417/10/19/6813)

## Install & deploy

**1. Clone this repository.**

```bash
git clone https://git.wur.nl/harta003/ara-ld.git
```

**2. Start Docker service(s).**

```bash
cd ara-ld
# build the image
docker build -t ara-ld .
# list available services
docker-compose config --services
# start all services or one-by-one
docker-compose up -d # or add [SERVICE]
```

